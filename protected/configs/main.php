<?php
return [
	'debug'=>true,
	//'debug'=>false,
	'app-settings'=>[
		'version' => '0.2.4',
		'debug'   => true,                  // prevents email sending AND increases log verbosity
		'deployment_notification_enabled'  => false, // false= all email notifications sent only to the developer
		'verbose_logging_enabled'          => true, // true=coding,testing,stabilization; false=production-stabilized
		'webhook_processing_enabled'       => false, // false= prevent all deploy processing. Typically used only while hotfixes are being tested.
		'local_repo_delete_enabled'        => true, // false is used during debug testing to allow tree comparisons via remote tools after the entire deploy process has completed
		'json_duplicate_checking_enabled'  => false,
		//'debug'   => false,
		'json-post-archive'      => '../protected/logs/incoming_json/', // lists path to the json input archive folder
		'json_archive_max_cnt'   => 50,                                 // lists the max number of json input files to store in the archive folder
		'process_log_file'       => '../protected/logs/gitlab-webhook-push.log',
		'target_site_backups'    =>'pre_deployment_archives/',
		//'target_site_backup_enabled'=>true,
		'target_site_backup_enabled'   =>false,
		'enforce-limited-access-rights'=>true,
		//'enforce-limited-access-rights'=>false, // set to false while debugging other features as this process takes several minutes
		//'access_rights_logging_enabled'=>true,  // produces approx 8,000 lines per deployment
		'access_rights_logging_enabled'=>false,

	],
	'self-update-repo'=>[
		'local_repo_name'          =>'stp--site-deploy',                    // $LOCAL_REPO_NAME in v0
		'local_repo_url_template'  =>'{$LOCAL_ROOT}/{$LOCAL_REPO_NAME}',    // you must keep the curly braces for the command line php to work correctly $LOCAL_REPO in v0
		'remote_repo_name'         =>'mvp',
		'remote_repo_url'          =>'git@gitlab:abc/mvp.git',              // $REMOTE_REPO in v0
	],    
    // @todo add multiple repos
    // @todo change the 'index-name' from 'git-repo' to 'git-repos'?
    // @todo can this setup actually handle multiple repos?
	'git-repo'=>[
		'local_repo_name'          =>'mvp',                                 // $LOCAL_REPO_NAME in v0
		'local_repo_url_template'  =>'{$LOCAL_ROOT}/{$LOCAL_REPO_NAME}',    // you must keep the curly braces for the command line php to work correctly $LOCAL_REPO in v0
		'remote_repo_name'         =>'mvp',
		'remote_repo_url'          =>'git@gitlab:abc/mvp.git',              // $REMOTE_REPO in v0
	],
	// <editor-fold defaultstate="collapsed" desc="sites">
	'sites'=>[
	[
		'site_label' =>'Staging 8095',
		'site_url'   =>'https://staging.example.com:8095/',
		'site_ip'    =>'45.55.69.139',
		'branch_name'=>'dev-frontend',                                      // $DESIRED_BRANCH in v0
		'web_root'   =>'/var/www/sites/',                                   // path   of $LOCAL_ROOT in v0
		'codebase'   =>'public_pNNNN_codebase',                             // folder of $LOCAL_ROOT in v0
		'deploy_stub'=>'public_p8095_deploy_stub',
		'site_root'  =>'public_p8095',
		'email_list' =>'production',
	],
	// alpha site for the dev-backend branch
	[
		'site_label' =>'Staging 8096',
		'site_url'   =>'https://staging.example.com:8096/',
		'site_ip'    =>'45.55.69.139',
		'branch_name'=>'dev-backend',
		'web_root'   =>'/var/www/sites/',
		'codebase'   =>'public_pNNNN_codebase',
		'deploy_stub'=>'public_p8096_deploy_stub',
		'site_root'  =>'public_p8096',
		'email_list' =>'staging8096Only',
	],	
	// alpha site for the oleksandr branch
	[
		'site_label' =>'Staging 8097',
		'site_url'   =>'https://staging.example.com:8097/',
		'site_ip'    =>'45.55.69.139',
		'branch_name'=>'sanjin',
		'web_root'   =>'/var/www/sites/',
		'codebase'   =>'public_pNNNN_codebase',
		'deploy_stub'=>'public_p8097_deploy_stub',
		'site_root'  =>'public_p8097',
		'email_list' =>'staging',
	],	
	// developer version (local to developer) of the dev-frontend site
	[
		'site_label' =>'Staging 8095',
		'site_url'   =>'http://abc-mvp:8103/',
		'site_ip'    =>'192.168.1.70',  // internal network ip address
		'branch_name'=>'dev-frontend',
		'web_root'   =>'/var/www/sites/',
		'codebase'   =>'public_pNNNN_codebase',
		'deploy_stub'=>'public_p8095_deploy_stub',
		'site_root'  =>'public_p8095',
		'email_list' =>'developer',
	],
	[
		'site_label'  =>'Staging 8090',
		'site_url'    =>'https://staging.example.com/',
		'site_ip'     =>'45.55.69.139',
		'branch_name' =>'dev-master',
		'web_root'    =>'/var/www/sites/',
		'codebase'    =>'public_pNNNN_codebase',
		'deploy_stub' =>'public_p8090_deploy_stub',
		'site_root'   =>'public_p8090',
		'email_list'  =>'production',
	],
	// developer version of the dev-master site
	[
		'site_label' =>'Staging 8090',
		'site_url'   =>'http://abc-mvp:8103/',
		'site_ip'    =>'192.168.1.70',  // internal network ip address
		'branch_name'=>'dev-master',
		'web_root'   =>'/var/www/sites/',
		'codebase'   =>'public_pNNNN_codebase',
		'deploy_stub'=>'public_p8090_deploy_stub',
		'site_root'  =>'public_p8090',
		'email_list' =>'developer',
	],
	[
		'site_label' =>'Production',
		'site_url'   =>'https://adm.example.com/',
		'site_ip'    =>'45.55.219.1',
		'branch_name'=>'master',
		'web_root'   =>'/var/www/sites/',
		'codebase'   =>'public_pNNNN_codebase',
		'deploy_stub'=>'public_p8090_deploy_stub',
		'site_root'  =>'public_p8090',
		'email_list' =>'production',
	],
	// developer version of the master site
	[
		'site_label' =>'Staging 8090',
		'site_url'   =>'http://abc-mvp:8103/',
		'site_ip'    =>'192.168.1.70', // internal network ip address
		'branch_name'=>'master',
		'web_root'   =>'/var/www/sites/',
		'codebase'   =>'public_pNNNN_codebase',
		'deploy_stub'=>'public_p8090_deploy_stub',
		'site_root'  =>'public_p8090',
		'email_list' =>'developer',
	],
	// Add a site for testing on the development server
	[
		'site_label' =>'Staging 8105',
		'site_url'   =>'http://staging.example.com:8105/',
		'site_ip'    =>'66.68.127.6', // external ip addr of temporary development server
		'branch_name'=>'webhook-testing',
		'web_root'   =>'/var/www/sites/',
		'codebase'   =>'public_pNNNN_codebase',
		'deploy_stub'=>'public_p8105_deploy_stub',
		'site_root'  =>'public_p8105',
		'email_list' =>'developer',
	],
],
// </editor-fold>
	// <editor-fold defaultstate="collapsed" desc="email settings">
	'emailSettings'=>[
		'logon-username'		=>'support@example.com',
		'logon-password'		=>'2]2NamfYW^o/',
		'generic-from'			=>'support@example.com',
		'errorDistributionList'	=>'support@example.com',

		'host'       => 'smtp.gmail.com',                   // Specify main and backup server
		'smtpAuth'   => true,                               // Enable SMTP authentication
		'smtpSecure' => 'tls',                              // Enable encryption, 'ssl' also accepted
		'smtpPort'   => 587,
	],
	// </editor-fold>
	// <editor-fold defaultstate="collapsed" desc="emailAddresses">
    /*
    	'emailAddresses' => [
		'developer'=>[
			'recipients'=>[
				['address'=>'admin@example.com','name'=>'Admin Name'],
			],
			'subjects'=>[
				'deployment skipped', 'deployment failed'
			],
			'msgBodies'=>[

			],
     * 
     */
                    
	'emailAddresses' => [
		'developer'=>[
			'recipients'=>[
				['address'=>'dana@example.com','name'=>'Dana Byrd'],
			],
			'subjects'=>[
				'deployment skipped', 'deployment failed'
			],
			'msgBodies'=>[

			],
		],
		'development'=>[
			'recipients'=>[
				['address'=>'dana@example.com','name'=>'Dana Byrd'],
			],
			'subjects'=>[

			],
			'msgBodies'=>[

			],
		],
		'staging'    =>[
			'recipients'=>[
				['address'=>'dana@example.com',  'name'=>'Dana Byrd'],
				//['address'=>'address3@somewhere.com',          'name'=>'fname lname'],
				//['address'=>'address4@gmail.com',              'name'=>'fname lname'],
			],
			'subjects'=>[

			],
			'msgBodies'=>[

			],

		],
		'staging8096Only'    =>[
			'recipients'=>[
				['address'=>'danabyrd@example.com',  'name'=>'Dana Byrd'],
				//['address'=>'address3@somewhere.com',          'name'=>'fname lname'],
				//['address'=>'address4@gmail.com',              'name'=>'fname lname'],
			],
			'subjects'=>[

			],
			'msgBodies'=>[

			],

		],		
		'production' =>[
			'recipients'=>[
				//['address'=>'address3@somewhere.com',          'name'=>'fname lname'],
				//['address'=>'address4@gmail.com',              'name'=>'fname lname'],
				//['address'=>'address3@somewhere.com',          'name'=>'fname lname'],
				//['address'=>'address4@gmail.com',              'name'=>'fname lname'],
			],
			'subjects'=>[

			],
			'msgBodies'=>[

			],
		],
	],
	// </editor-fold>
];

