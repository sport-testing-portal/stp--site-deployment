<?php
//defined('SYSPATH') or die('No direct script access.');
// * Project.........: EPF (Enterprise Process Framework)
// * Purpose.........: Enable custom exception to behave as well as default exceptions.
// * CEC.............: asp-2.1.1
// * Initial Release.: v0.1.0
// * Author..........: Dana A Byrd
// * History.........:
//     2013-08-28 0.0.1.2 (cec) dbyrd: Add test harness and cec to header.
// * Notes...........:
//     @todo: test harness this or move it to a feature branch.

// Here's a test that shows that all information is properly preserved throughout the backtrace.
/* Uncomment the function below to test the error class */
/*
function exceptionTest()
{
    try {
        throw new TestException();
    }
    catch (TestException $e) {
        echo "Caught TestException ('{$e->getMessage()}')\n{$e}\n";
    }
    catch (Exception $e) {
        echo "Caught Exception ('{$e->getMessage()}')\n{$e}\n";
    }
}
*/
//echo '<pre>' . exceptionTest() . '</pre>';
/**/




//Here's a sample output:
//
//Caught TestException ('Unknown TestException')
//TestException 'Unknown TestException' in C:\xampp\htdocs\CustomException\CustomException.php(31)
//#0 C:\xampp\htdocs\CustomException\ExceptionTest.php(19): CustomException->__construct()
//#1 C:\xampp\htdocs\CustomException\ExceptionTest.php(43): exceptionTest()
//#2 {main}
?>


<?php
/*
 * Pulled code base from here: http://php.net/manual/en/language.exceptions.php
 * author =  ask@nilpo.com 2009-05-27 07:19
 * class TestException extends CustomException {};
 *
 * dbyrd: You can also use $e->getTraceAsString() in the catch. I don't know what
 *  that output looks like yet, but it sounds cool.
 *
 * Some good ideas regarding errors... pulled from "the tor network" faq page
 * There are five log levels (also called "log severities") you might see in Tor's logs:

    * "err": something bad just happened, and we can't recover. Tor will exit.
    * "warn": something bad happened, but we're still running. The bad thing might be a bug in the code, some other Tor process doing something unexpected, etc. The operator should examine the message and try to correct the problem.
    * "notice": something the operator will want to know about.
    * "info": something happened (maybe bad, maybe ok), but there's nothing you need to (or can) do about it.
    * "debug": for everything louder than info. It is quite loud indeed.
 */

interface IException
{
    /* Protected methods inherited from Exception class */
    public function getMessage();                 // Exception message
    public function getCode();                    // User-defined Exception code
    public function getFile();                    // Source filename
    public function getLine();                    // Source line
    public function getTrace();                   // An array of the backtrace()
    public function getTraceAsString();           // Formated string of trace

    /* Overrideable methods inherited from Exception class */
    public function __toString();                 // formated string for display
    public function __construct($message = null, $code = 0);
}

abstract class CustomException extends Exception implements IException
{
    protected $message = 'Unknown exception';     // Exception message
    private   $string;                            // Unknown
    protected $code    = 0;                       // User-defined exception code
    protected $file;                              // Source filename of exception
    protected $line;                              // Source line of exception
    private   $trace;                             // Unknown

    public function __construct($message = null, $code = 0)
    {
        if (!$message) {
            throw new $this('Unknown '. get_class($this));
        }
        parent::__construct($message, $code);
    }

    public function __toString()
    {
        return get_class($this) . " '{$this->message}' in {$this->file}({$this->line})\n"
                                . "{$this->getTraceAsString()}";
    }
}


class Util_Debug_ContextReader {
    private static function the_trace_entry_to_return() {
        $trace = debug_backtrace();

        for ($i = 0; $i < count($trace); ++$i) {
            if ('debug' == $trace[$i]['function']) {
                if (isset($trace[$i + 1]['class'])) {
                    return array(
                        'class' => $trace[$i + 1]['class'],
                        'line' => $trace[$i]['line'],
                    );
                }

                return array(
                    'file' => $trace[$i]['file'],
                    'line' => $trace[$i]['line'],
                );
            }
        }

        return $trace[0];
    }

    /**
     * @return string
     */
    public function current_module() {
        $trace_entry = self::the_trace_entry_to_return();

        if (isset($trace_entry['class']))
            return 'class '. $trace_entry['class'];
        else
            return 'file '. $trace_entry['file'];

        return 'unknown';
    }

    public function current_line_number() {
        $trace_entry = self::the_trace_entry_to_return();
        if (isset($trace_entry['line'])) return $trace_entry['line'];
        return 'unknown';
    }
}
?>



