<?php
/**
 * Sends emails to various distribution lists using the central config file
 * @version 0.1.1
 */
require_once '../protected/extensions/PHPMailer/class.phpmailer.php';
class notification {

	private static $initialized = false;

	public static $config       = null;
	public static $debug        = false;
	public static $verbose      = null;
	//public static $logfile      = '../protected/logs/gitlab-webhook-push.log';
	public static $logfile      = ''; // get the value from the central config file
	public static $version      = null;

	public static $deployment_notification_enabled = null;


    /**
     * Construct won't be called inside this class and is uncallable from
     * the outside. This prevents instantiating this class.
     * This is by purpose, because we want a static class.
     */
    private function __construct() {}

    private static function initialize()
    {
        if (self::$initialized){
            return;
		}
		self::$config  = self::getConfig();
		self::$debug   = self::$config['app-settings']['debug'];
		self::$verbose = self::$config['app-settings']['verbose_logging_enabled'];
		self::$logfile = self::$config['app-settings']['process_log_file'];
		self::$version = self::$config['app-settings']['version'];
		self::$deployment_notification_enabled = self::$config['app-settings']['deployment_notification_enabled'];
		self::$initialized = true;
    }

	private static function getConfig(){
		// Load main config
		$fileMainConfig = '../protected/configs/' . 'main.php';
		if (!file_exists($fileMainConfig)) {
			throw new \Exception('Cannot find main config file "' . $fileMainConfig . '".');
		}
		$configMain = require($fileMainConfig);
		return $configMain;
	}

	/**
	 *
	 * @param string $emailList 'developer','development','staging','production'
	 * @param string $message Html email body
	 */
	public static function send($emailList, $message=''){
		self::initialize();

		$msgFooter = self::msgFooter();

		//echo 'require succeeded <br>';
		$config = self::$config['emailSettings'];
		$addrs  = self::$config['emailAddresses'];


		$mail = new PHPMailer;

		$mail->IsSMTP();                                  // Set mailer to use SMTP
		$mail->Host       = $config['host'];              // Specify main and backup server
		$mail->SMTPAuth   = $config['smtpAuth'];          // Enable SMTP authentication
		$mail->Username   = $config['logon-username'];    // SMTP username
		$mail->Password   = $config['logon-password'];    // SMTP password
		$mail->SMTPSecure = $config['smtpSecure'];        // Enable encryption, 'tls' or 'ssl' also accepted
		$mail->Port       = $config['smtpPort'];

		$mail->From       = $config['generic-from'];
		$mail->FromName   = 'GSM Site Deployment';

		//if (self::$config['debug'] == true){
		$debug = self::$debug;
		if ($debug == true){
			$emailList = 'developer';
		}
		if (self::$deployment_notification_enabled == false){
			$emailList = 'developer';
		}
		foreach ($addrs[$emailList]['recipients'] as $addr){
			//$mail->AddAddress('ellen@example.com', 'fname lname');   // Add a recipient
			$mail->AddAddress($addr['address'], $addr['name']);
		}

	//	$mail->AddAddress('ellen@example.com');               // Name is optional
	//	$mail->AddReplyTo('info@example.com', 'Information');
	//	$mail->AddCC('cc@example.com');
	//	$mail->AddBCC('bcc@example.com');

		$mail->WordWrap = 50;                                 // Set word wrap to 50 characters
	//	$mail->WordWrap = 70;                                 // Set word wrap to 70 characters
	//	$mail->AddAttachment('/var/tmp/file.tar.gz');         // Add attachments
	//	$mail->AddAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
		$mail->IsHTML(true);                                  // Set email format to HTML

		$mail->Subject = 'Deployment Notification';
		//$mail->Body    = 'This is the HTML message body <b>in bold!</b>';
		//$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

		$mail->Body    = $message . $msgFooter;
		//$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

		if(!$mail->Send()) {
		   echo 'Message could not be sent.';
		   echo 'Mailer Error: ' . $mail->ErrorInfo;
		   self::logAppend('Mailer Error: ' . $mail->ErrorInfo);
		   exit;
		}
		//echo 'Message has been sent';
		self::logAppend('Deployment message has been sent to the ' . $emailList. ' mailing list');
	}

	/**
	 *
	 * @param string $branchName
	 * @see self::send()
	 */
	public static function sendToDeveloper($branchName, $noteType, $body=null){

		self::initialize();

		$msgFooter = self::msgFooter();

		if ($noteType == 'Deployment Skipped'){
			$subject  = 'Deployment Skipped Notification';
			$body     = "Site deployment was skipped for branch <b>$branchName</b>"
					  . $msgFooter;
			$altBody  = "Site deployment was skipped for branch $branchName";
			$fromName = $noteType;
		} elseif ($noteType == 'Deployment Skipped for duplicate json'){
			$subject  = 'Deployment Skipped Notification';
			$body     = "Site deployment was skipped for branch <b>$branchName</b> because a duplicate json file was sent."
					  . $msgFooter;
			$altBody  = "Site deployment was skipped for branch $branchName because a duplicate json file was sent.";
			$fromName = $noteType;
		} elseif ($noteType == 'Deployment Details'){
			$subject  = $noteType;
			$body     = $body . $msgFooter;
			$altBody  = strip_tags($body);
			$fromName = $noteType;
		} else {
			$subject  = $noteType;
			$body     = $body . $msgFooter;
			$altBody  = strip_tags($body);
			$fromName = 'Deployment';
		}
		//echo 'require succeeded <br>';
		$config = self::$config['emailSettings'];
		$addrs  = self::$config['emailAddresses'];

		$mail = new PHPMailer;

		$mail->IsSMTP();                                  // Set mailer to use SMTP
		$mail->Host       = $config['host'];              // Specify main and backup server
		$mail->SMTPAuth   = $config['smtpAuth'];          // Enable SMTP authentication
		$mail->Username   = $config['logon-username'];    // SMTP username
		$mail->Password   = $config['logon-password'];    // SMTP password
		$mail->SMTPSecure = $config['smtpSecure'];        // Enable encryption, 'tls' or 'ssl' also accepted
		$mail->Port       = $config['smtpPort'];

		$mail->From       = $config['generic-from'];
		$mail->FromName   = $fromName;

		$emailList = 'developer';

		foreach ($addrs[$emailList]['recipients'] as $addr){
			$mail->AddAddress($addr['address'], $addr['name']);
		}

		$mail->WordWrap = 50;                                 // Set word wrap to 50 characters
		$mail->IsHTML(true);                                  // Set email format to HTML

		$mail->Subject = $subject;
		$mail->Body    = $body;
		$mail->AltBody = $altBody;

		if(!$mail->Send()) {
		   echo 'Message could not be sent.';
		   echo 'Mailer Error: ' . $mail->ErrorInfo;
		   //exit;
		   return false;
		}

		echo 'Message has been sent';
		return true;
	}

	/**
	 *
	 * @param string $message
	 *
	 */
	public static function logAppend($message)
	{
		self::initialize();
		$date = date('Y-m-d H:i:s');
		$pre  = $date . ' (' . $_SERVER['REMOTE_ADDR'] . '): ';

        $search   = array("\r\n", "\n\n", "\r");
        $replace = '';
        // Processes \r\n's first so they aren't converted twice.
        $cleaned_msg = str_replace($search, $replace, $message);
		//file_put_contents($this->logfile, $pre . $message . "\n", FILE_APPEND);
		if (!empty($cleaned_msg)){
			file_put_contents(self::$logfile, $pre . $cleaned_msg . "\n", FILE_APPEND);
		}
	}

	/**
	 * Generates a standard message footer for any deployment server that identifies the server
	 * @internal Development Status = Golden!
	 * @version 0.1.0
	 * @since 0.2.0
	 */
	public static function msgFooter(){

		self::initialize();

		$serverName = $_SERVER['SERVER_NAME'];
		$serverAddr = $_SERVER['SERVER_ADDR'];
		$externalIpAddress = self::hostIpAddress();

		if (trim($serverName) == trim($externalIpAddress)){
			$serverAddr = $serverName .':' . $serverAddr; // eg external ip address : internal ip address
			$serverName = "Dana's development server";
		}

		$appVersion = self::$version;

		$msgFooter = "<br><br><span style='font-family:sans; color:grey;'>"
				. "Sent from $serverName at $serverAddr running version $appVersion of <b>gsm/deploy</b>"
				. "</span><br>";
		return $msgFooter;
	}

	/**
	 * Fetch the public facing ip address.
	 * @return string $ip_address
	 * @version 0.1.1 of method
	 * @since 0.2.2 of class
	 * @internal Development Status = ready for testing (rft)
	 */
	public static function hostIpAddress(){
		self::initialize();
		$opts = array(
			'http' => array(
				'method'=>"GET",
				'header'=>"Content-Type: text/html; charset=utf-8"
			)
		);
		$context = stream_context_create($opts);
		// fetch the external public-facing IP address
		$html = file_get_contents('http://checkip.dyndns.com',false, $context);
		if (is_bool($html)){
			// retry up to ten times
			$i = 0;
			while ( is_bool($html) && $i < 10) {
				$html = file_get_contents('http://checkip.dyndns.com',false, $context);
				$i++;
			}
		}
		if (is_bool($html)){
			return '';
		}
		// since true html is being returned we need to strip the tags
		$text = strip_tags($html);
		//$ipAddress = str_ireplace("Current IP Address: ", "", $text); // cli version
		$ipAddress = str_ireplace("Current IP CheckCurrent IP Address: ", "", $text); // strip tags version

		if (self::isValidIP($ipAddress)){
			return $ipAddress;
		} else {
			return '';
		}
		return $ipAddress;
	}

	/**
	 * Validate a string as an IP address formatted string. Does not resolve the address it merely verifies the format.
	 * @param type $ip
	 * @return boolean
	 * @internal Development Status = Golden!
	 */
	public static function isValidIP($ip){
		self::initialize();
		if(preg_match("^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}^", $ip))
		{
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Fetch the public facing ip address.
	 * @return string $ip_address
	 * @version 0.1.0 of method
	 * @deprecated since v0.2.1 of class
	 */
	public static function hostIpAddressUsingCLI(){
		self::initialize();
		if (isCli() == false){
			return false;
		}
		$url  = "http://checkip.dyndns.org/";
		$html = file_get_contents($url);
		$ipAddress = str_ireplace("Current IP Address: ", "", $html);
		return $ipAddress;
	}
}
