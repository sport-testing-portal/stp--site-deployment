<?php

/**
 * Alias for add_trailing_slash()
 */
function addSlash($path){
	return add_trailing_slash($path);
}

/**
 *
 * @param string $str
 * @return string
 */
function add_trailing_slash($str){
	$str .= (substr($str, -1) == '/' ? '' : '/');
	return $str;
}

/**
 * Alias for hostIpAddress()
 * @return string $ip_address
 *
 */
function get__host_ip_address(){
	return hostIpAddress();
}

/**
 * Fetch the public facing ip address.
 * @return string $ip_address
 * @version 0.1.1 of method
 * @since 0.2.2 of class
 */
function hostIpAddress(){
	$opts = array(
		'http' => array(
			'method'=>"GET",
			'header'=>"Content-Type: text/html; charset=utf-8"
		)
	);
	$context = stream_context_create($opts);
	// fetch the external public-facing IP address
	$html = file_get_contents('http://checkip.dyndns.com',false, $context);
	$ipAddress = str_ireplace("Current IP Address: ", "", $html);
	return $ipAddress;
}

// File Functions *************************************************************
/** delete a file
 * @param string $fileUri
 * @return string
 * @internal typically used to delete the json log files
 */
function fileDelete($fileUri, $return_int_yn="N"){

	$parm1_tag   = "(*$fileUri*)";
	$parm2_tag   = "(*$return_int_yn*)";
	$failure_tag = "File deletion skipped.";

	$scenario    = array();  // Scenarios and return values matrix: scenario[index,array(integer, string)].
	$scenario[1] = array(-1, "file_uri is a required parameter. The parameter value was empty $parm1_tag! $failure_tag");  // blank parameter passed
	$scenario[2] = array(-1, "The value passed for return_int_yn is invalid $parm2_tag. $failure_tag");                    // bogus parameter passed
	$scenario[3] = array( 0, "The file was not found $parm1_tag. $failure_tag");                                           // bogus parameter passed
	$scenario[4] = array( 0, "The file deletion failed $parm1_tag. $failure_tag");                                         // intended operation failed
	$scenario[5] = array( 1, "File deleted successully $parm1_tag.");                                                      // intended operation succeed

	// Assert the parameters passed in.
	if ( empty($fileUri) ){
		error_Handler(__METHOD__, __LINE__,              $scenario[1][1]);
		return ($return_int_yn=="Y") ? $scenario[1][0] : $scenario[1][1];   // bad parm passed
	}
	if ( empty($return_int_yn) ){
		$return_int_yn = "N";                                               // set default value

	} elseif (strtoupper($return_int_yn) != "Y" && strtoupper($return_int_yn) != "N"){
		error_Handler(__METHOD__, __LINE__,              $scenario[2][1]);
		return ($return_int_yn=="Y") ? $scenario[2][0] : $scenario[2][1];   // bad parm passed
	}

	// Assert that the file exists
	if ( file_exists($fileUri) ) {
		unlink($fileUri);                                                  // delete file
	} else {
		return ($return_int_yn=="Y") ? $scenario[3][0] : $scenario[3][1];   // file not found
	}

	// Assert that the file was deleted.
	if ( file_exists($fileUri) ) {
		return ($return_int_yn=="Y") ? $scenario[4][0] : $scenario[4][1];   // delete failed
	} else {
		return ($return_int_yn=="Y") ? $scenario[5][0] : $scenario[5][1];   // delete succeeded!
	}
}


/**
 * Get an array of files
 * @param string $sourceFolder     The location of your files
 * @param string $ext              File extension you want to limit to (i.e.: *.txt)
 * @param int    $epocSecs         If you only want files that are at least so old.
 * @param int    $maxFileCnt       Number of files you want to return
 * @param bool   $ignoreSubFolders Ignore sub folders
 * @return array[]
 * @internal return structure ['basename','ext','filename','size','updated_dt','created_dt','accessed_dt','sha256','file_uri']
 * @author   alan@synergymx.com, debugged and rewritten by Dana Byrd <dana@globalsoccermetrix.com>
 * @internal source = http://php.net/manual/en/control-structures.break.php
 * @internal Development Status = Golden!
 * @internal typically called json logs management
 */
function globFiles($sourceFolder, $ext=null, $epocSecs=null, $maxFileCnt=null, $ignoreSubFolders=false){
	// dana: php glob() appears to prefer, ie only works with a physical file path on some websites.
	//   so I've added a reference to SERVER doc root and also added the automatic
	//   appendage of a trailing slash where needed as glob also seems to require the trailing
	//   slash, but this is unconfirmed as yet.
	// Sample output
	//  [path] => c:\temp\my_videos\
	//  [name] => fluffy_bunnies.flv
	//  [size] => 21160480
	//  [date] => 2007-10-30 16:48:05

	if( !is_dir( $sourceFolder ) ) {
		$msg =  "Invalid directory! (*$sourceFolder*)\n\n";
		return $msg;
	}
	// add a trailing slash
	if (! stripos($sourceFolder, '*') !== false){
		$globPath    = add_trailing_slash($sourceFolder)."*"; // append '/*' to the path
	} else {
		// a wild card file skeleton is already included in the path. Use the path as is.
		$globPath    = $sourceFolder;
	}

	$filesList    = glob($globPath, GLOB_MARK); // glob_mark adds trailing slash to the folders returned
	$set_limit    = 0;
	$out		  = [];

	$hash_enabled_yn = 'Y';
	$targetFileUri = "";
	foreach($filesList as $key => $fileUri) {

		if( !is_null($maxFileCnt) && $set_limit == $maxFileCnt ){
			// maximum file count has been reached
			break;
		}
		if( $ignoreSubFolders && substr($fileUri, -1) == '/'){
			// a trailing slash indicates a sub folder
			continue;
		}

		if(is_null($epocSecs) || filemtime( $fileUri ) > $epocSecs ){
			$targetFileUri = $fileUri;
			if ($hash_enabled_yn == "Y"){
				$hash256 = hash_file('sha256', $targetFileUri);
			}
			$path_parts = pathinfo( $fileUri );
			if ( !empty($ext) && $path_parts['extension'] !== $ext){
				continue;
			}
			$excludeFiles = ['.gitignore','.gitkeep'];
			if ( array_search($path_parts['filename'], $excludeFiles) !== false){
				continue;
			}

			$out[$key]['path']          = $path_parts['dirname'];
			$out[$key]['basename']      = $path_parts['basename'];  // includes extension
			$out[$key]['ext']           = ( (array_key_exists('extension', $path_parts)) ? $path_parts['extension'] : '');
			$out[$key]['filename']      = $path_parts['filename'];  // sans extension
			$out[$key]['size']          = filesize( $fileUri );
			$out[$key]['updated_dt']    = date('Y-m-d G:i:s', filemtime( $fileUri ) );
			$out[$key]['created_dt']    = date('Y-m-d G:i:s', filectime( $fileUri ) );
			$out[$key]['accessed_dt']   = date('Y-m-d G:i:s', fileatime( $fileUri ) );
			$out[$key]['sha256']        = $hash256;
			$out[$key]['file_uri']      = $fileUri;

			$set_limit++;
		}
	}
	if(!empty($out)){
		return $out;
	} else {
		// die( "No files found!\n\n" );
		$msg =  "No files found! (*$sourceFolder*)\n\n";
	}
}

function in_multiarray($elem, $array){
	// Author: cousinka at gmail dot com
	// Source Code: http://php.net/manual/en/function.in-array.php
	// Works with object properties as well.
	// if the $array is an array or is an object
	 if( is_array( $array ) || is_object( $array ) )
	 {
		 // if $elem is in $array object
		 if( is_object( $array ) )
		 {
			 $temp_array = get_object_vars( $array );
			 if( in_array( $elem, $temp_array ) ){
				 return true;
			 }
		 }

		 // if $elem is in $array return true
		 if( is_array( $array ) && in_array( $elem, $array ) ){
			 return true;
		 }


		 // if $elem isn't in $array, then check foreach element
		 foreach( $array as $array_element )
		 {
			 // if $array_element is an array or is an object call the in_multiarray function to this element
			 // if in_multiarray returns TRUE, than return is in array, else check next element
			 if( ( is_array( $array_element ) || is_object( $array_element ) ) && in_multiarray( $elem, $array_element ) )
			 {
				 return true;
			 }
		 }
	 }

	 // if isn't in array return FALSE
	 return FALSE;
}

function copyTree($fromPath='',$toPath='', $writeEnabled=false){

//	if ($writeEnabled)
//	{
//		// Write changes
//		$RSYNC_CMD="/usr/bin/rsync -a -c "
//		."--partial --copy-links --delete --force --compress --human-readable "
//		."'$fromPath' "
//		."'$toPath'";
//	} elseif ($writeEnabled == false) {
//		// Read only
//		$RSYNC_CMD="/usr/bin/rsync -a -c "
//		."--dry-run --copy-links --partial --verbose --delete --force --compress --human-readable "
//		."--itemize-changes "
//		."'$fromPath' "
//		."'$toPath'";
//		//#--delete
//	} else {
//		throw new Exception('Invalid writeEnabled flag', 'copyTree()', 'functions.php');
//	}

	//$inlcludeSymlinks = '--copy-links ';
	$inlcludeSymlinks = '';
	if ($writeEnabled)
	{
		// Write changes
		$RSYNC_CMD="/usr/bin/rsync -a -c "
		."--partial $inlcludeSymlinks --delete --force --compress --human-readable "
		."'$fromPath' "
		."'$toPath'";
	} elseif ($writeEnabled == false) {
		// Read only
		$RSYNC_CMD="/usr/bin/rsync -a -c "
		."--dry-run $inlcludeSymlinks --partial --verbose --delete --force --compress --human-readable "
		."--itemize-changes "
		."'$fromPath' "
		."'$toPath'";
		//#--delete
	} else {
		throw new Exception('Invalid writeEnabled flag', 'copyTree()', 'functions.php');
	}

	shell_exec("{$RSYNC_CMD}");
//	$output = [];
//	$return_var = false;
//	exec($RSYNC_CMD, $output, &$return_var);
//	return $output;
	// return the rsync used in case there is an error.
	return $RSYNC_CMD;
}

// Date and File Functions Intersection ****************************************
function date_to_filename($date=NULL) {
	// Returns the current datetime as a valid file name
	$dateFormat = 'Y-m-d_His';
	if ( empty($date)){
		$timeStamp = time();
	} else {
		$timeStamp = strtotime($date);
	}
	$dateTime= date($dateFormat,$timeStamp);
	return $dateTime;
}

// CLI Functions (PHP running in the Unix/Linux shell *************************
function isCli() {
	return php_sapi_name()==="cli";
}

/**
 * clears the screen, like "clear screen"
 * @param type $out
 * @return string
 */
  function clearscreen($out = true) {
    $clearscreen = chr(27)."[H".chr(27)."[2J";
    if ($out){
		print $clearscreen;
	} else{
		return $clearscreen;
	}
  }

/**
 *
 * @param type $argv
 * @return string
 * @example php myscript.php --user=nobody --password=secret -p --access="host=127.0.0.1 port=456"
 *
 * Array
 * (
 *     [user] => nobody
 *     [password] => secret
 *     [p] => true
 *     [access] => host=127.0.0.1 port=456
 * )
 *
 * @see http://php.net/manual/en/features.commandline.php
 *
 */
function arguments($argv) {
    $_ARG = array();
	$matches = [];
    foreach ($argv as $arg) {
		if (preg_match('/--([^=]+)=(.*)/',$arg,$matches)) {
			$_ARG[$matches[1]] = $matches[2];
		} elseif(preg_match('/-([a-zA-Z0-9])/',$arg,$matches)) {
            $_ARG[$matches[1]] = 'true';
        }
    }
	return $_ARG;
}

/**
 * For those of you who want the old CGI behaviour that changes to the actual directory of the script use:
 */
function changeDirectory(){

	chdir(dirname($_SERVER['argv'][0]));
}