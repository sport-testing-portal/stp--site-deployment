#!/usr/bin/php
<?php
/**
 * Steps needed to deploy
 * 1. Archive the protected folder
 * 2. Clone repo into the local codebase folder
 * 3. Checkout the branch to deploy
 * 4. Get the current mode setting
 * 5. Rsync/Copy the protected folder to the host site
 * 6. Restore the mode setting
 * 7. Clear the repo files from the local codebase folder
 * 8. Send notification
 *
 * @example php -f clone.repo.php --branch="master"
 * @example php clone-repo.php --branch="master"
 * @internal Development Status = Golden!
 * @since 0.0.1
 * @version 0.1.0
 * @todo pull all var values from the global config to remove all hard coded vars
 * @todo consider passing a unique site identifier to pull site info from the global config to remove all hard coded values
 *
 */
$debug=false;
//$debug=true;
if ($debug){
	echo "loading " . __FILE__ . "\n";
	$eh_status = error_reporting(E_ALL);
	ini_set('display_errors', TRUE);
	ini_set('display_startup_errors', TRUE);
}

if ($debug){
	clearscreen();
}
$argsParsed			= arguments($argv);
$DESIRED_BRANCH     = $argsParsed['branch'];

if ($debug){
	echo "desired branch = **$DESIRED_BRANCH**" . PHP_EOL;
}
//$config = getConfig();


// Init vars
//$GIT_WRAPPER_URI    = "/var/www/sites/gsm/dev/deploy/protected/cli/git.sh -i ";
$GIT_WRAPPER_URI    = addSlash(pathinfo(__FILE__, PATHINFO_DIRNAME)) . 'git.sh -i ';
$SSH_KEY_URI        = "/var/www/.ssh/gitlab-mvp-deployment-key";
$GIT_CMD            = $GIT_WRAPPER_URI . $SSH_KEY_URI;
$LOCAL_ROOT         = "/var/www/sites/public_pNNNN_codebase";
$LOCAL_REPO_NAME    = "mvp";
$LOCAL_REPO         = "{$LOCAL_ROOT}/{$LOCAL_REPO_NAME}";
$REMOTE_REPO        = "git@gitlab:gsm/mvp.git";
//$DESIRED_BRANCH     = "master";
//$DESIRED_BRANCH     = "dev-master";
//$DESIRED_BRANCH     = "dev-backend";
//$DESIRED_BRANCH     = "dev-frontend";
//$DESIRED_BRANCH     = "develop";
//$DESIRED_BRANCH     = "feat-xyz";
//$DESIRED_BRANCH     = "hotfix-xyz";

// Delete local repo if it exists
if (file_exists($LOCAL_REPO)) {
    shell_exec("rm -rf {$LOCAL_REPO}");
}

// Clone fresh repo from github using desired local repo name and checkout the desired branch
// echo shell_exec("cd {$LOCAL_ROOT} && git clone {$REMOTE_REPO} {$LOCAL_REPO_NAME} && cd {$LOCAL_REPO} && git checkout {$DESIRED_BRANCH}");
// git.sh -i ssh-key-file git-command
$cmdResult = shell_exec("cd {$LOCAL_ROOT} && {$GIT_CMD} clone {$REMOTE_REPO} {$LOCAL_REPO_NAME} && cd {$LOCAL_REPO} && git checkout {$DESIRED_BRANCH}");
echo $cmdResult . PHP_EOL;

//echo("done " . time(). "\n");
die();



function getConfig(){
	// Load main config
	$fileMainConfig = '../protected/configs/' . 'main.php';
	if (!file_exists($fileMainConfig)) {
		throw new \Exception('Cannot find main config file "' . $fileMainConfig . '".');
	}
	$configMain = require($fileMainConfig);
	return $configMain;
}

/**
 * Delete local repo if it exists
 * @param string $localRepo local URI of the git repository
 */
function deleteLocalCodebaseRepo($localRepo){
	if (file_exists($localRepo)) {
		shell_exec("rm -rf {$localRepo}");
	}
}

/**
 *
 * @param string $argv
 * @return string
 * @example php myscript.php --user=nobody --password=secret -p --access="host=127.0.0.1 port=456"
 *
 * Array
 * (
 *     [user] => nobody
 *     [password] => secret
 *     [p] => true
 *     [access] => host=127.0.0.1 port=456
 * )
 *
 * @see http://php.net/manual/en/features.commandline.php
 *
 */
function arguments($argv) {
    $_ARG = array();
	$matches = [];
    foreach ($argv as $arg) {
		if (preg_match('/--([^=]+)=(.*)/',$arg,$matches)) {
			$_ARG[$matches[1]] = $matches[2];
		} elseif(preg_match('/-([a-zA-Z0-9])/',$arg,$matches)) {
            $_ARG[$matches[1]] = 'true';
        }
    }
	return $_ARG;
}



/**
 * clears the screen, like "clear screen"
 * @param type $out
 * @return string
 */
function clearscreen($out = true) {
    $clearscreen = chr(27)."[H".chr(27)."[2J";
    if ($out){
		print $clearscreen;
	} else{
		return $clearscreen;
	}
}

/**
 * Add a trailing slash to a directory name
 * @param string $str
 * @return string
 * @see addSlash and add_trailing_slash in protected/components/functions.php
 */
function addSlash($str){
	$str .= (substr($str, -1) == '/' ? '' : '/');
	return $str;
}