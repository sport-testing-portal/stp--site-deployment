#! /bin/bash

# Remember to impersonate the apache2 daemon user before running this script
# sudo su - -s /bin/bash www-data

# Initialize a server for git branch clones via ssh.
cd /var/www/sites/public_pNNNN_codebase

# Examples
# /var/www/sites/deploy/protected/cli/git.sh -i /var/www/.ssh/gitlab-mvp-deployment-key git clone git@git.somegitrepo.com:group/project.git localProjectName
# /var/www/sites/deploy/protected/cli/git.sh -i /var/www/.ssh/gitlab-site_01-deployment-key git clone git@git.yourRemoteRepo.com:group/projectName.git localProjectName

/var/www/sites/stp--site-deployment/protected/cli/git.sh -i /var/www/.ssh/gitlab-mvp-deployment-key git clone git@git.example.com:gsm/mvp.git mvp

