<?php
/**
 * Deploy via GitLab Web Hook
 *
 * Steps to determine if a deploy is warranted
 * 1. Loop through site to see if there is a hosting site for the branch in the web hook
 *
 * Steps needed to deploy
 * 1. Archive the protected folder
 * 2. Clone repo into the local codebase folder
 * 3. Checkout the branch to deploy
 * 4. Get the current mode setting
 * 5. Rsync/Copy the protected folder to the host site
 * 6. Restore the mode setting
 * 7. Clear the repo files from the local codebase folder
 * 8. Send notification
 *
 * This hook uses php's exec() function, so make sure it can be executed.
 * See http://php.net/manual/function.exec.php for more info
 *
 * @internal This file is a mirror of mvp.php
 * @example http://localhost:8104/deploy.php?XDEBUG_SESSION_START=netbeans-xdebug&p=Mos97VYEj4PvRXNYWmbS
 * @example  http://66.68.127.6:8104/deploy.php?p=Mos97VYEj4PvRXN
 * @todo move the procedural code at the top of this file into a deploy class method.
 */

// allow debugging of the deploy procedural code at the top of this file, NOT the deploy object itself
$debug=true;
if ($debug){
	echo 'loading deployment class<br>';
	$eh_status = error_reporting(E_ALL);
	ini_set('display_errors', TRUE);
	ini_set('display_startup_errors', TRUE);
}

require_once '../protected/components/error.class.php';
require_once '../protected/components/functions.php';
require_once '../protected/components/notification.php';


$serverName = $_SERVER['SERVER_NAME'];
$serverAddr = $_SERVER['SERVER_ADDR'];

$deploy = new \deploy();
$deploy->deploy();

/**
 * Class used to deploy STP source code
 */
class deploy{
    public static $initialized    = false;
    public $fileMainConfig = '../protected/configs/main.php';
    public $conf           = [];        // main configuration array
    public $date_str       = null;
    public $log_file       = null;
    public $site           = null;
    public $web_root       = null;

    public $json_archive         = null; // lists path to the json input archive folder
    public $json_archive_max_cnt = 50;   // lists the max number of json input files to store in the archive folder
    public $json_archive_deleted = [];   // lists files deleted by exceeding the max archive count

    public $target_site_protected_folder = null; // has a trailing slash
    public $target_site_archive_folder   = null; // has a trailing slash
    public $repo_protected_folder        = null; // has a trailing slash

    public $debug        = false;
    public $verbose      = null;
    public $local_repo_delete_enabled = null;

    // @todo move hard coded properties to the config file
    public $hookfile     = '../protected/cli/gitlab-webhook-push.sh';
    public $logfile      = '../protected/logs/gitlab-webhook-push.log';

    public $error_fatal = false;
    public $errors      = [];
    public $warnings    = [];
    public $dupes       = [];

    public $deployedAppVersionNumberShort   = '';
    public $deployedAppVersionNumberLong    = '';
    public $deployedAppVersionNumberVerbose = '';

    //Mos97VYEj4PvRXNYWmbSPQSWSoY5VA // 30 char // chopped it to 25
    //$password = 'Mos97VYEj4PvRXNYWmbSPQSWS';
    //$password = 'Mos97VYEj4PvRXNYWmbS'; // shorten for testing
    public $password = 'Mos97VYEj4PvRXN'; // shorten for testing
    // @todo ? consider creating a function to get refs by useCase
    public $ref      = array(
            'refs/heads/master',
            'refs/heads/dev-master',
            'refs/heads/dev-frontend',
            'refs/heads/dev-backend',
            'refs/heads/develop',
    );

	/**
	 *
	 * @return type
	 * @@internal Development Status = Golden!
	 */
    public function __construct()
	{
        if (self::$initialized){
            return;
		}
		$this->conf         = $this->getConfig();
		$this->date_str     = date_to_filename();
		// Handle json archive settings
		$this->json_archive         = addSlash($this->conf['app-settings']['json-post-archive']) . 'json__' . $this->date_str;
		$this->json_archive_max_cnt = $this->conf['app-settings']['json_archive_max_cnt'];

		$this->verbose = $this->conf['app-settings']['verbose_logging_enabled'];
		$this->local_repo_delete_enabled = $this->conf['app-settings']['local_repo_delete_enabled'];

		self::$initialized  = true;
	}

	/**
	 * Fetch the configuration array and popluate the conf property using the values from config file array
	 * @return type
	 * @throws Exception
	 * @verson 0.1.0
	 * @since 0.1.0
	 * @internal Development Status = Golden!
	 */
	public function getConfig()
	{
		// Load main config
		$fileMainConfig = $this->fileMainConfig;
		if (!file_exists($fileMainConfig)) {
			throw new \Exception('Cannot find main config file "' . $fileMainConfig . '".');
		}
		$configMain = require($fileMainConfig);
		$this->conf = $configMain;
		return $configMain;
	}

	/**
	 *
	 * @internal Development Status = Golden!
	 * @todo reset all site permissions to the most restrictive eg files at 500
	 * @internal Development Status = Golden!
	 */
	public function setAccessRights($site){

		if (! $this->conf['app-settings']['enforce-limited-access-rights']){
			$msg = "setAccessRights() is being skipped because it is disabled in configs/main.php"
					. "\n"
					. str_repeat(' ',40) ."see: [app-settings][enforce-limited-access-rights]";
			$this->logAppend($msg);
			return;
		}

		if (!file_exists($this->target_site_protected_folder)){
			return;
		}elseif (file_exists($this->target_site_protected_folder)){
			$target = $this->target_site_protected_folder;
		} else {
			$target = addSlash($site['web_root']) . addSlash($site['site_root']) . 'protected';
		}

		$this->logAppend('Launching restrict-access shell script...');
		$cliFolder = 'protected/cli/';
		$cliPath   = str_ireplace('public', $cliFolder, __DIR__);
		$cliName   = 'restrict_access_nosudo.php --target="'
				//. $site['branch_name']
				.$target
				.'"';

		$shellFileUri = $cliPath . $cliName;
		echo $shellFileUri;

		if ($this->conf['app-settings']['access_rights_logging_enabled']){
			$logShellOutput=true;
		} else {
			// logging is disabled
			$logShellOutput=false;
		}
		$this->exec_command('php ' . $shellFileUri, $logShellOutput);
		$this->logAppend('Shell based restrict-access script finished');
	}

	/**
	 *
	 * @internal Development Status = code construction
	 * @deprecated since version 0.1.0
	 */
	public function setAccessRights_v0(){

		if (! $this->conf['app-settings']['enforce-limited-access-rights']){
			$msg = "setAccessRights() is being skipped because it is disabled in configs/main.php"
					. "\n"
					. str_repeat(' ', 40) . "see: [app-settings][enforce-limited-access-rights]";
			$this->logAppend($msg);
			return;
		}

		$owner='www-data';  // root
		$group='www-data';  // root
		$path= $this->target_site_protected_folder;
		$recursive='-R';
		$folderOrdinal= 700;
		$fileOrdinal  = 600;
		$this->changeOwner($owner, $group, $path, $recursive);
		$this->changePermissions('d', $folderOrdinal, $path); // depth defaults to 20
		$this->changePermissions('f', $fileOrdinal, $path); // depth defaults to 20
	}

	/**
	 * archive json for playbacks and debugging
	 * @param string $input
	 * @version 0.1.2
	 * @since 0.1.2
	 */
	public function jsonArchiveInput($input)
	{
		$date_str  = $this->date_str;
		$conf      = $this->conf;
		// @todo should the next line be hanled by the contructor?
		$this->json_archive = addSlash($conf['app-settings']['json-post-archive']) . 'json__' . $date_str;
		$this->writeToFile($this->json_archive, $input);

		$this->jsonDeleteExpiredInputLogFiles();
	}

	/**
	 * delete json files in ascending date order for any file over the max json log file count
	 * @return int The count of files removed
	 * @version 0.1.1
	 * @since 0.1.2
	 */
	public function jsonDeleteExpiredInputLogFiles()
	{

		$this->jsonDeleteDuplicateInputFiles();
		$jsonArchiveFolder = $this->conf['app-settings']['json-post-archive'];

		$jsonArchive = globFiles($jsonArchiveFolder);
		// order files by date
		$jsonArchiveSorted = $this->array_sort($jsonArchive, 'created_dt', SORT_ASC);
		$maxAllowedFileCnt = $this->json_archive_max_cnt;  // populated by __construct()
		$fileCnt = 0;
		$deleted = [];
		foreach($jsonArchiveSorted as $file){
			$fileCnt++;
			if ($fileCnt > $maxAllowedFileCnt){
				$deleted[] = $file;
				fileDelete($file['file_uri']);
			}
		}

		if (count($deleted) > 0){
			$this->json_archive_deleted['deleted over max count files'] = $deleted;
			return count($deleted);
		}
	}

	/**
	 * delete json files in ascending date order for any file over the max json log file count
	 * @returns int $fileDeletedCnt
	 * @internal called by: jsonDeleteExpiredInputLogFiles()
	 */
	public function jsonDeleteDuplicateInputFiles()
	{
		$jsonArchiveFolder = $this->conf['app-settings']['json-post-archive'];
		$jsonFiles = globFiles($jsonArchiveFolder);
		// order files by date
		$jsonFilesSorted = $this->array_sort($jsonFiles, 'created_dt', SORT_ASC);
		$deleted = [];
		foreach ($jsonFilesSorted as $key => $file) {
			//$created = $file['created_dt'];
			$sha256  = $file['sha256'];
			// order files by date
			$dupes = $this->getArrayDuplicates($jsonFilesSorted, $dupeKeyName='sha256', $sha256, 'created_dt');
			if (count($dupes) > 0){
				$this->dupes = $dupes;
				foreach ($dupes as $dupeKey=>$dupe){
					if ($dupeKey != $key){
						$dupeUri = $dupe['file_uri'];
						$deleted[$dupeKey] = $dupe;
						fileDelete($dupeUri);
					}
				}
			}
		}
		if (count($deleted) > 0){
			$this->json_archive_deleted['deleted duplicate files'] = $deleted;
			return count($deleted);
		}
	}

	public function incomingJsonIsDuplicate($jsonInput){
		$jsonArchiveFolder = $this->conf['app-settings']['json-post-archive'];
		$jsonFiles  = globFiles($jsonArchiveFolder);
		$sha256		= hash('sha256', $jsonInput);

		$matches = [];
		foreach ($jsonFiles as $innerFile){
			if ($innerFile['sha256'] == $sha256){
				$matches[] = $innerFile;
				// return true;
			}
		}

		if (count($matches) > 0){
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Find duplicate values in a two dimensional array
	 * @param type $sourceArray
	 * @param type $dupeKeyName
	 * @param type $dupeKeyValue
	 */
	public function getArrayDuplicates($sourceArray, $dupeKeyName, $dupeKeyValue, $sortKey){
		$out = [];
		foreach ($sourceArray as $key => $item){
			if ($item[$dupeKeyName] == $dupeKeyValue){
				$out[$key] = $item;
			}
		}
		$sortedOut = $this->array_sort($out, $sortKey);
		return $sortedOut;
	}

	public function array_sort($array, $on, $order=SORT_ASC)
	{
		$new_array = array();
		$sortable_array = array();

		if (count($array) > 0) {
			foreach ($array as $k => $v) {
				if (is_array($v)) {
					foreach ($v as $k2 => $v2) {
						if ($k2 == $on) {
							$sortable_array[$k] = $v2;
						}
					}
				} else {
					$sortable_array[$k] = $v;
				}
			}

			switch ($order) {
				case SORT_ASC:
					asort($sortable_array);
				break;
				case SORT_DESC:
					arsort($sortable_array);
				break;
			}

			foreach ($sortable_array as $k => $v) {
				$new_array[$k] = $array[$k];
			}
		}

		return $new_array;
	}

	/**
	 * archive the protected folder of the target deployment site
	 * @param array[] $site
	 * @internal Development Status = Golden!
	 * @todo enable backup for specific branches by making the backup-enabled setting a site-config attribute in configs/main.php
	 *
	 */
	public function archiveProtectedFolder($site)
	{
		if (empty($this->web_root)){
			$this->web_root = addSlash($site['web_root']);
		}
		$w_root = $this->web_root;
		$s_root = addSlash($site['site_root']);

		// example: /var/www/sites/public_p8095/protected
		$this->target_site_protected_folder = $w_root . $s_root . 'protected';

		// example: /var/www/sites/pre_deployment_archives/2015-08-06_160146/
		$this->target_site_archive_folder = $w_root . addSlash($this->conf['app-settings']['target_site_backups'])
				. addSlash($this->date_str);
		// example: /var/www/sites/pre_deployment_archives/public_p8090/2015-08-06_160146/
		// rsync can't make more than one top level folder eg it can't handle mkdir -p
		$parentArchiveFolder = $w_root . addSlash($this->conf['app-settings']['target_site_backups'])
				. $site['site_root'];
		if (!file_exists($parentArchiveFolder)){
			$this->exec_command("mkdir -p $parentArchiveFolder");
		}

		$this->target_site_archive_folder = $w_root . addSlash($this->conf['app-settings']['target_site_backups'])
				. $site['site_root'] . DIRECTORY_SEPARATOR . addSlash($this->date_str);

		if ($this->conf['app-settings']['target_site_backup_enabled']){
			$source = $this->target_site_protected_folder;
			$target = $this->target_site_archive_folder;
			$msg = "archiveProtectedFolder()"
					. "\n"
					. "    source: $source"
					. "\n"
					. "    target: $target"
					;
			$this->logAppend($msg);
			if (!file_exists($source)){
				$this->logAppend("    The source folder was NOT found!");
			} else {
				// the source folder exists
				copyTree($source, $target, $writeEnabled=true);
			}
		} else {
			$msg = "archiveProtectedFolder() is being skipped because it is disabled in configs/main.php"
					. "\n"
					. str_repeat(' ', 40) . "see: [app-settings][target_site_backup_enabled]";
			$this->logAppend($msg);
		}
	}

	/**
	 * Email the technical details of the deployment to the developer
	 * @internal Development Status = ready for testing (rft)
	 */
	public function sendDeploymentObjectDetails()
	{
		// json file name
		// protected archive folder
		$tableHeader = "Here are the technical details of the deployment object <br>";
		$row1 = '<tr><td>JSON File</td><td>'. basename($this->json_archive) .'</td></tr>';
		$row2 = '<tr><td>Site Archive</td><td>'. $this->target_site_archive_folder .'</td></tr>';
		$table   = '<table>{{ROWS}}</table>';
		$body = $tableHeader . str_ireplace('{{ROWS}}', $row1 . $row2, $table);

		$site = $this->site;
		$branchName = $site['branch_name'];
		notification::sendToDeveloper($branchName, 'Deployment Details', $body);
	}

	/**
	 *
	 * @return boolean
	 * @internal Development Status = ready for testing (rft)
	 */
	public function deleteLocalCodebaseRepo($site)
	{
		$conf = $this->conf;
//		if($conf['debug']){
//			return false;
//		}
		$w_root   = addSlash($site['web_root']);
		$codebase = addSlash($site['codebase']);
		$repo_uri = $w_root . $codebase . $conf['git-repo']['local_repo_name'];


		// Delete local repo if it exists
		if (file_exists($repo_uri)) {
			shell_exec("rm -rf {$repo_uri}");
		}
	}

	/**
	 *
	 * @param array $site
	 * @internal Development Status = ready for testing (rft)
	 * @version 0.1.3
	 * @since 0.1.0
	 */
	public function copyFilesFromRepoToSite($site){
		if ($this->error_fatal){
			$msg = "ERROR: copyFilesFromRepoToSite() found a fatal error and quit before updating any files." ;
			$this->logAppend($msg);
			return;
		}
		if (empty($this->web_root)){
			$this->web_root = addSlash($site['web_root']);
		}
		$w_root = $this->web_root;
		if ($this->debug){
			$s_root = addSlash($site['deploy_stub']);
		} else {
			$s_root = addSlash($site['site_root']);
		}

		$repo_root = addSlash($site['codebase']);
		// example: /var/www/sites/public_pNNNN_codebase
		//$repo_site_protected_folder = $w_root . $repo_root . 'mvp/protected';
		// the trailing slash tells rsync to copy the contents of the folder, but not the folder itself
		//$repo_site_protected_folder = $w_root . $repo_root . 'mvp/protected/';
		$localRepoName = $this->conf['git-repo']['local_repo_name' ];
		$repo_site_protected_folder = $w_root . $repo_root . addSlash($localRepoName) . 'protected/';

		// example: /var/www/sites/public_p8095/protected
		//$this->target_site_protected_folder = $w_root . $s_root . 'protected/';
		$this->target_site_protected_folder = $w_root . $s_root . 'protected';

		$source = $repo_site_protected_folder;
		//$target = $this->target_site_protected_folder;
		$target = $w_root . $s_root . 'protected';
		// verify that local repo exists
		if (! file_exists($repo_site_protected_folder)) {
			$msg = 'ERROR: local repo folder doesnt exist. copyFilesFromRepoToSite() aborted before making any file system changes.';
			$this->error_fatal = true;
			$this->errors[] = ['severity'=>'fatal', 'error_message'=>$msg];
			$this->logAppend($msg);
			notification::sendToDeveloper($site['branch_name'], "local repo missing"
					, "local repo doesnt exist ($repo_site_protected_folder)");
			return false;
		}
		// $source = "/var/www/sites/public_pNNNN_codebase/mvp/protected"
		// $target = "/var/www/sites/public_p8090/protected/"
		$rsyncCmd = copyTree($source, $target, $writeEnabled=true);


		// @todo add rsync based folder compare after hashdeep tree compare method is added
		//rsync --archive --dry-run --checksum --verbose /source/directory/ /destination/directory
		//$compareResult = $this->compareTreesViaMd5($source, $target);
		$compareResult = $this->compareTreesViaHashdeep($source, $target);
		if ($compareResult){
			$this->logAppend("rsync of local repo's protected folder to the " . $site['site_label'] . " site's protected folder successful");
		} else {
			$this->logAppend("ERROR: rsync of local repo's protected folder to the " . $site['site_label'] . " site's protected folder FAILED");
			$this->logAppend($rsyncCmd);
			$logShellOutput = true;
			$this->exec_command($rsyncCmd, $logShellOutput);
		}
	}

	/**
	 * Use grep to md5 all program file content throughout an entire directory tree
	 * @param string $path
	 * @return array
	 * @internal note that grep can only 'read' files that have a 'readible' permission
	 * @todo add a cd before the grep, otherwise it will fail
	 */
	public function getMd5Sum($path) {
		$cmd = "grep -aR -e . {$path} | md5sum | cut -c-32";
		$output = array();

		exec($cmd, $output);
		$md5sum = $output[0];
		return $md5sum;
	}

	/**
	 *
	 * @param type $path
	 * @return bool $foldersMatch
	 * @internal Development Status = ready for testing (rft)
	 */
	public function compareTreesViaMd5($path1, $path2) {
            $md5sum1 = $this->getMd5Sum($path1);
            $md5sum2 = $this->getMd5Sum($path2);
            $output = [];
            $output[$path1] = $md5sum1;
            $output[$path2] = $md5sum2;

            if ($md5sum1 === $md5sum2){
                    $foldersMatch = true;
            } else {
                    $foldersMatch = false;
            }
            return $foldersMatch;
	}

	/**
	 *
	 * @param string $repoPath The local git REPO protected folder path
	 * @param string $sitePath The local SITE protected folder path
	 * @return bool
	 * @internal sudo hashdeep -v -l -r -a -k /var/www/sites/gsm/dev/hashdeep_output.txt /var/www/sites/public_p8090/protected/
	 * @internal Development Status = code construction
	 * @see http://md5deep.sourceforge.net/hashdeep.html
	 * @see https://linhost.info/2010/05/using-hashdeep-to-ensure-data-integrity/
	 */
	public function compareTreesViaHashdeep($repoPath, $sitePath) {

		$logFilePathReal = realpath($this->logfile);
		$logPath = addSlash(pathinfo($logFilePathReal, PATHINFO_DIRNAME));

		// create a hashdeep file for each path to allow compares (audits) from both directions
		// -r = recursive, -l (lowercase L) use only relative paths otherwise any audit will totally fail 100% every time.
		$repoHashdeepOutputUri = $logPath . 'repo_hashdeep_output.txt';
		$siteHashdeepOutputUri = $logPath . 'site_hashdeep_output.txt';
		// @example hashdeep -r -l ./ > /var/www/sites/gsm/dev/hashdeep_output.txt
		$hashRepoCmd = "cd {$repoPath} && hashdeep -r -l ./ > {$repoHashdeepOutputUri}";
		$hashSiteCmd = "cd {$sitePath} && hashdeep -r -l ./ > {$siteHashdeepOutputUri}";
		$logShellOutput = false;
		$this->exec_command($hashRepoCmd, $logShellOutput);
		$this->exec_command($hashSiteCmd, $logShellOutput);
//
//		// shallow audit eg failed | succeeded @todo what is the actual wording? (called a non-verbose audit)
//		// @example hashdeep -r -l -a -k /var/www/sites/gsm/dev/hashdeep_output.txt ./
//		// $this->exec_command("cd {$repoPath} && hashdeep -r -l -a -k {$siteHashdeepOutputUri} ./");
		// shallow audit
		$shallowAuditCmd = "cd {$repoPath} && hashdeep -r -l -a -k {$siteHashdeepOutputUri} ./";
		$auditOutput = $this->exec_command("{$shallowAuditCmd}");

		if (trim($auditOutput) == 'hashdeep: Audit passed'){
			$foldersMatch = true;
		} else {
			$foldersMatch = false;
		}

		if ($foldersMatch == false) {
			// summary audit lists matching file count and mismatched file count (called a verbose audit)
			$auditSiteCmd = "cd {$repoPath} && hashdeep -v -r -l -a -k {$siteHashdeepOutputUri} ./";
			$this->logAppend($auditSiteCmd);
			$this->exec_command($auditSiteCmd);
			$auditRepoCmd = "cd {$sitePath} && hashdeep -v -r -l -a -k {$repoHashdeepOutputUri} ./";
			$this->logAppend($auditRepoCmd);
			$this->exec_command($auditRepoCmd);
		}

		return $foldersMatch;
/*

			$foldersMatch = false;
			// summary audit lists matching file count and mismatched file count (called a verbose audit)
			$this->exec_command("cd {$repoPath} && hashdeep -v -r -l -a -k {$siteHashdeepOutputUri} ./");
			$this->exec_command("cd {$sitePath} && hashdeep -v -r -l -a -k {$repoHashdeepOutputUri} ./");
			// negative compare (lists missing files) eg files in the hash file that are not in the folder being tested against?
			$repoHashdeepNegAuditOutputUri = $logPath . 'repo_hashdeep_negative_audit_output.txt';
			$siteHashdeepNegAuditOutputUri = $logPath . 'site_hashdeep_negative_audit_output.txt';
			$this->exec_command("cd {$repoPath} && hashdeep -v -r -l -x -k {$siteHashdeepOutputUri} ./ > {$repoHashdeepNegAuditOutputUri}");
			$this->exec_command("cd {$sitePath} && hashdeep -v -r -l -x -k {$repoHashdeepOutputUri} ./ > {$siteHashdeepNegAuditOutputUri}");
		} else {
			$foldersMatch = true;
		}
*/
		// example
		// sudo hashdeep -v -l -r -a -k /var/www/sites/gsm/dev/hashdeep_output.txt /var/www/sites/public_p8090/protected/
		// negative compare
		//hashdeep -v -l -r -x -k /var/www/sites/gsm/dev/hashdeep_output.txt ./

		// positive compare uses the -m switch rather than -x
		// hashdeep -v -l -r -m -k /var/www/sites/gsm/dev/hashdeep_output.txt ./
	}

	/**
	 * Use git clone over ssh using the deploy key in /var/www/.ssh
	 * @param type $site
	 * @internal Development Status = Golden!
	 *
	 */
	public function cloneRepoIntoCodebaseFolder($site){
            $this->logAppend('Launching shell hook script...');
            $cliFolder = 'protected/cli/';
            $cliPath   = str_ireplace('public', $cliFolder, pathinfo(__FILE__, PATHINFO_DIRNAME));
            $cliName   = 'clone_repo.php --branch="' . $site['branch_name'] .'"';

            $hookUri = $cliPath . $cliName;
            echo $hookUri;

            //$hookfile     = '/var/www/sites/gsm/dev/deploy/protected/cli/clone-repo.php --branch="' . $site['branch_name'] .'"';
            //$this->exec_command('php ' . $hookfile);
            $this->exec_command('php ' . $hookUri);
            //$this->log_append('Shell based git pull script finished');

            $w_root        = addSlash($site['web_root']);
            $repo_root     = addSlash($site['codebase']);
            $localRepoName = $this->conf['git-repo']['local_repo_name' ];

            $repo_site_protected_folder = $w_root . $repo_root . addSlash($localRepoName) . 'protected';

            // verify that local repo exists
            if (! file_exists($repo_site_protected_folder)) {
                    // The git clone failed
                    $this->logAppend("git repo cloning script failed");
            } else {
                    $this->logAppend('git repo cloning was successful');
            }
	}

	/**
	 * Copy the mode file from a previously deployed site overwriting the repo mode file
	 * @param array[] $site
	 * @return array[]
	 * @version 0.1.1
	 * @internal Development Status = Golden!
	 *
	 */
	public function getAndSetSiteModeSetting($site)
	{
            $w_root   = addSlash($site['web_root']);
            $s_root   = addSlash($site['site_root']);
            $codebase = addSlash($site['codebase']);
            $site_mode_uri = $w_root . $s_root   . 'protected/config/mode.php';
            $local_repo_name = $this->conf['git-repo']['local_repo_name'];
            $repo_mode_uri = $w_root . $codebase
                            . addSlash($local_repo_name)
                            . 'protected/config/mode.php';

            $repo_mode_val_bfr = file_get_contents($repo_mode_uri);
            $site_mode_val_bfr = file_get_contents($site_mode_uri);

            // Delete local repo if it exists
            if (file_exists($site_mode_uri)) {
                    // use a non aliased version of cp so the -i switch is bypassed
                    $cmd = '/bin/cp --force --verbose "' . $site_mode_uri .'" "' .$repo_mode_uri . '"';
                    //$cmdResult = shell_exec("/bin/cp -f '$site_mode_uri' '$repo_mode_uri' ");
                    //$cmdResult2 = shell_exec("$cmd");
                    $cmdResult = shell_exec("{$cmd}");
                    $this->logAppend(trim($cmdResult));
            }
            $repo_mode_val_aft = file_get_contents($repo_mode_uri);

            if (
                            ($repo_mode_val_bfr !== $repo_mode_val_aft)
                            &&
                            ($repo_mode_val_bfr !== $site_mode_val_bfr)
                    )
            {
                    $this->logAppend("site mode configuration was successful");
            } else {
                    $this->logAppend("site mode configuration failed");
            }
            return [
                    'repo_mode_val_bfr' =>$repo_mode_val_bfr,
                    'site_mode_val_bfr' =>$site_mode_val_bfr,
                    'repo_mode_val_aft' =>$repo_mode_val_aft,
                    ];
	}

	/**
	 *
	 * @param string $message
	 * @internal Development Status = Golden!
	 */
	public function logAppend($message)
	{
		$date = date('Y-m-d H:i:s');
		$pre  = $date . ' (' . $_SERVER['REMOTE_ADDR'] . '): ';

        $search   = array("\r\n", "\n\n", "\r");
        $replace = '';
        // Processes \r\n's first so they aren't converted twice.
        $cleaned_msg = str_replace($search, $replace, $message);
		//file_put_contents($this->logfile, $pre . $message . "\n", FILE_APPEND);
		if (!empty($cleaned_msg)){
			file_put_contents($this->logfile, $pre . $cleaned_msg . "\n", FILE_APPEND);
		}
	}

	/**
	 * Return html for use in a message body
	 * @param array[] $site An array with the properties of the server site being deployed
	 * @return string $html
	 * @internal Development Status = Golden!
	 */
	public function parseMessageHeaderHtml($site){
		$siteName   = $site['site_label'];
		$branchName = $site['branch_name'];
		$siteUrl    = $site['site_url'];
		$siteVer    = $this->getVersionNumber($site);

//		$siteNameTemplate   = "<h3>Our $siteName Site</h3> has been updated by a commit to our ". "\n";
//		$branchNameTemplate = "gitlab <strong>" .$branchName . "</strong>" . "\n"
//			. " branch and has been automatically deployed by our gitlab web hook handler. You can view the site updates here "	;
//		$siteUrlTemplate    = "<a href='$siteUrl' target='_blank'>$siteName</a><br><br>" . "\n";

		$siteNameTemplate   = "<h3>Our $siteName Site</h3> has been updated by a commit to our ". "\n";
		$branchNameTemplate = "gitlab <strong>" .$branchName . "</strong>" . "\n"
			. " branch ($siteVer) and has been automatically deployed by our gitlab web hook handler. You can view the site updates here "	;
		$siteUrlTemplate    = "<a href='$siteUrl' target='_blank'>$siteName</a><br><br>" . "\n";

		$messageHeaderHtml  = $siteNameTemplate . $branchNameTemplate . $siteUrlTemplate;
		return $messageHeaderHtml;
	}

	/**
	 * Return html for use in a message body
	 * @param type $data
	 * @return string $html
	 * @internal Development Status = Golden!
	 */
	public function parseCommitsAsHtml($data){
            $tableHeader = "You can view the code changes in the individual commits by clicking the links below <br>";
            $rowHtml = '<tr><td><a href="{{URL}}" target="_blank"><span style="font-family:mono;">{{LABEL}}</span></a></td><td>{{MESSAGE}}</td></tr>';
            $search  = ['{{URL}}','{{LABEL}}','{{MESSAGE}}'];

            $table   = '<table>{{ROWS}}</table>';
            $rows=[];
            foreach ($data['commits'] as $commit){
                    $url   = $commit->url;
                    $label = substr($commit->id, 0, 9);
                    $msg   = $commit->message;
                    $replace = [$url, $label, $msg];

                    $row    = str_ireplace($search, $replace, $rowHtml);
                    $rows[] = $row;
            }
            $html = str_ireplace('{{ROWS}}', implode("\n",$rows), $table);
            return $tableHeader. $html;
	}

	/**
	 *
	 * @param type $owner
	 * @param type $group
	 * @param type $path
	 * @param type $recursive
	 * @internal consider calling a shell script owned by root to do the update eg chmod 4750 see: setAccessRights()
	 * @internal called by setAccessRights_v0()
	 * @deprecated since version 0.1.0
	 */
	public function changeOwner($owner='www-data',$group='www-data',$path='',$recursive='-R')
	{
            //eval "sudo chown -R www-data:www-data $LOCAL_COPY_TARGET_PATH"
            $bashCmd = "sudo chown --changes $recursive $owner:$group $path";
            $cmdResult = shell_exec("{$bashCmd}");
            echo $cmdResult . PHP_EOL;
	}

	/**
	 *
	 * @param type $objectType
	 * @param type $ordinal
	 * @param type $path
	 * @param type $depth
	 * @internal consider calling a shell script owned by root to do the update eg chmod 4750 see: setAccessRights()
	 * @internal called by setAccessRights_v0()
	 * @deprecated since version 0.1.0
	 */
	public function changePermissions($objectType='d',$ordinal=0,$path='', $depth=20)
	{
            // eval "cd $LOCAL_COPY_TARGET_PATH; find -maxdepth 20 -type d -exec sudo chmod 700 {} \;"
            $bashCmd = 'cd '.$path . '; find -maxdepth ' .$depth.' -type ' .$objectType.' -exec sudo chmod --changes ' .$ordinal .' {} \;';
            $cmdResult = shell_exec("{$bashCmd}");
            echo $cmdResult . PHP_EOL;
	}

	/**
	 * Test collecting all output rather than
	 * @param string $command
	 * @param bool $logShellOutput
	 * @version 0.1.1
	 * @since 0.2.2
	 * @internal Development Status = Golden!
	 */
	public function exec_command($command,$logShellOutput=true)
	{
            $output = array();
            $output[] = shell_exec($command);
            if($logShellOutput == false){
                    if (count($output) == 1){
                            if ( empty( trim($output[0]) ) ){
                                    return '';
                            }
                    } else {
                            $msg = count($output) . ' shell output lines skipped via app-settings';
                            $this->logAppend('SHELL: ' . $msg);
                    }
                    if (count($output) > 0){
                            return end($output);
                    }
            }

            foreach ($output as $line) {
                    $this->logAppend('SHELL: ' . trim($line));
            }
            if (count($output) > 0){
                    return trim(end($output));
            }
	}

	/**
	 *
	 * @param type $command
	 * @deprecated since version 0.1.0
	 */
	public function exec_command_v0($command)
	{
            $output = array();

            exec($command, $output);

            foreach ($output as $line) {
                    $this->logAppend('SHELL: ' . $line);
            }
	}

	/**
	 *
	 * @param string $branchName A branch name from the json post data
	 * @param string $serverAddr The ip address of the currently running deployment class
	 * @return array[]
	 * @internal Development Status = Golden!
	 * @version 0.1.1
	 * @since 0.2.0
	 */
	public function deploymentSite($branchName, $serverAddr){

            $sites = $this->conf['sites'];
            foreach($sites as $site){
                    if ( ($site['branch_name'] == $branchName) && ($serverAddr == $site['site_ip']) ){
                            $this->site = $site;
                            return $site;
                    }
            }
            return [];
	}

	/**
	 *
	 * @param array[] $sites A list of all deployment sites
	 * @param type $branchName A branch name from the json post data
	 * @deprecated since version 0.2.1
	 */
	public function deploymentSite_v0($sites, $branchName){
            foreach($sites as $site){
                    if ($site['branch_name'] == $branchName){
                            return $site;
                    }
            }
            return [];
	}

	/**
	 *
	 * @return array[]
	 * @internal used to validate the content of a mode file, but currently unused.
	 * @internal Development Status = Golden!
	 */
	public function getValidModes()
	{
            return array(
                    100 => 'DEVELOPMENT',
                    200 => 'TEST',
                    300 => 'STAGING',
                    400 => 'PRODUCTION'
            );
	}

	/**
	 * Write a summary of the process status to the log file and send notifications to the developer
	 * @version 0.1.0
	 * @since 0.2.3
	 * @internal called by $this->deploy()
	 */
	public function processStatusSummary() {
		$errCount  = count($this->errors);
		$warnCount = count($this->warnings);

		if ($errCount > 0){
			$errHtml  = '<b>Deployment Errors</b><br>' . $this->arrayToHtmlViaVarDump($this->errors);
		} else {
			$errHtml  = '';
		}

		if ($warnCount > 0){
			$warnHtml  = '<b>Deployment Warnings</b><br>' . $this->arrayOfWarningsToHtml($this->warnings);
		} else {
			$warnHtml  = '';
		}

		$logMsgSuffix = "\n"; // add a blank line as the last line of batch process items recorded in the in the log
		if ($errCount > 0 && $warnCount > 0){
			$logMsg = "Deployment operations completed with ERRORS({$errCount}) and WARNINGS({$warnCount})";
			$html   = "Deployment operations completed with <b>errors ({$errCount})</b> and <b>warnings ({$warnCount})</b><br>"
					. $errHtml . $warnHtml;
			$this->logAppend($logMsg . $logMsgSuffix);
			notification::sendToDeveloper('all branches', 'errors logged', $html);

		} elseif ($errCount > 0){
			$logMsg = "Deployment operations completed with ERRORS({$errCount})";
			$html   = "Deployment operations completed with <b>errors ({$errCount})</b><br>"
					. $errHtml ;
			$this->logAppend($logMsg . $logMsgSuffix);
			notification::sendToDeveloper('all branches', 'errors logged', $html);

		} elseif ($warnCount > 0){
			$logMsg = "Deployment operations completed with WARNINGS({$warnCount})";
			$html   = "Deployment operations completed with <b>warnings ({$warnCount})</b><br>"
					. $warnHtml;
			$this->logAppend($logMsg . $logMsgSuffix);
			notification::sendToDeveloper('all branches', 'warnings logged', $html);

		} else {
			$this->logAppend('Deployment operations completed succesfully' . $logMsgSuffix);
		}
	}

	/**
	 * Main function of this class. This is an orchistration method.
	 */
	public function deploy() {

		$serverAddr = $_SERVER['SERVER_ADDR'];

		if ($this->conf['app-settings']['webhook_processing_enabled'] !== true){
			$this->logAppend('webhook processing enabled is disabled');
			die();
		}

		// validate password or end app
		$this->auth();

		// GitLab sends the json as raw post data
		$input = file_get_contents("php://input");
		$json  = json_decode($input);

		if (!is_object($json) || empty($json->ref)) {
			$this->logAppend('Invalid push event data');
			die();
		}

		// ref is a base property of the json object and the deploy object
		if (isset($this->ref))
		{
			$_refs = (array) $this->ref;
			if ($json->ref !== '*' && !in_array($json->ref, $_refs)) {
				$this->logAppend('Ignoring ref ' . $json->ref);
				die();
			}
		}

		// get commit info
		// pull branch data like author and commit notes
		$authorOfCommit = $json->user_name;
		$branchName     = $this->getBranchFromRef($json->ref);
		$commits        = $json->commits;
		$data=[
			'author' =>$authorOfCommit,
			'branch' =>$branchName,
			'commits'=>$commits,    // id, message, timestamp, url
			'count'  =>$json->total_commits_count,
		];
		if($this->verbose){
			$this->logAppend("Branch received from webhook: $branchName");
		}
		// determine if a site should be updated
		$site   = $this->deploymentSite($branchName, $serverAddr);
		if (count($site,COUNT_RECURSIVE) == 0){
			// deployment canceled. Notify site deployment developer
			$this->logAppend("Site config for $branchName was not found on this server. Deployment aborted.");
			notification::sendToDeveloper($branchName, 'Deployment Skipped');
			die();
		}

		// test for duplicate json data
		$jsonIsDupe  = $this->incomingJsonIsDuplicate($input);
		if ($this->conf['app-settings']['json_duplicate_checking_enabled'] == false){
			if ($jsonIsDupe){
				$jsonDupeOverrideMessage = "WARNING: jsonIsDupe is true and being overridden by the application configuration."
						. "\n"
						. str_repeat(' ', 40) ."see [app-settings][json_duplicate_checking_enabled]";
				$this->warnings[] = $jsonDupeOverrideMessage;
				$this->logAppend($jsonDupeOverrideMessage);
				$jsonIsDupe = false;
			}
		}

		if ($jsonIsDupe){
			// deployment canceled. Notify site deployment developer
			notification::sendToDeveloper($branchName, 'Deployment Skipped for duplicate json');
			die();
		}
		// archive json for playbacks and debugging
		$this->jsonArchiveInput($input);

		// archive the protected folder of the target deployment site
		$this->logAppend('Launching deployment process...');
		$this->archiveProtectedFolder($site); // approx 9 secs
		$this->cloneRepoIntoCodebaseFolder($site);
		// overwrite the repo's mode file using the existing site's mode file
		$this->getAndSetSiteModeSetting($site);
		// rsync the repo files using the --delete --force options to remove obsolete site files
		$this->copyFilesFromRepoToSite($site);
		// set repo's files folder/file owner to www-data and set the least privlidge permissions on the repo files
		$this->setAccessRights($site);
		// remove the repo files the server's drive to limit exposure from a security perspective
		if ($this->local_repo_delete_enabled){
			$this->deleteLocalCodebaseRepo($site);
		} else {
			$msg_repo_del_skip = 'WARNING: Skipped post-deploy local-repo deletion due to app config';
			$this->warnings[] = $msg_repo_del_skip;
			$this->logAppend($msg_repo_del_skip);
		}

		// Disable email from my development server
		if ($serverAddr !== '192.168.1.70'){
			// build deployment site url
			$messageHead = $this->parseMessageHeaderHtml($site);      // returns site name, branch name, site url
			$messageBody = $this->parseCommitsAsHtml($data);          // convert commits info into an html table

			notification::send($site['email_list'], $messageHead . $messageBody);
			$this->sendDeploymentObjectDetails();
		} else {
			$this->logAppend('Deployment email skipped while running on the development server');
		}
		if (count($this->warnings) > 0 && $serverAddr !== '192.168.1.70'){
			// @todo send warnings to developer
			echo 'send warnings to developer <br>';
			notification::sendToDeveloper('all branches', 'warnings logged', 'warnings were encountered during deployment');
		}

		$this->processStatusSummary();
	}


	/**
	 *
	 * @param type $array
	 * @param type $encode_html_entities
	 * @throws Exception
	 * @version 0.1.1
	 * @since 0.2.3
	 * @internal Development Status = Golden!
	 */
	public function arrayOfWarningsToHtml($array){
		// Display easy-to-read valid PHP array code
		$html = '<ul>{{items}}</ul>'."\n";
		$itemCnt = 0;
		$items = '';
		foreach ($array as $item){
			if( !is_string($item)){
				throw new Exception("Item should be a string, not an array", "is_string(item)", __FUNCTION__);
			}
			$itemCnt++;
			$items .= '<li>' . htmlentities($item) . '</li>'."\n";
		}
		$out = str_ireplace('{{items}}', $items, $html);
		return $out;
	}

	/**
	 *
	 * @param type $array
	 * @param type $encode_html_entities
	 * @throws Exception
	 * @version 0.1.1
	 * @since 0.2.3
	 * @internal Development Status = Golden!
	 * @internal the output is not pretty, but it does work
	 */
	public function arrayToHtmlViaVarDump($array, $encode_html_entities=true){
		// Display easy-to-read valid PHP array code
		if ( $encode_html_entities == true) {
			$html = '<pre>' . htmlentities(var_export( $array, true )) . '</pre>';
		} elseif ( $encode_html_entities == false) {
			$html = '<pre>' . var_export( $array, true ) . '</pre>';
		} else {
			$bt   = debug_backtrace();
			//$file_name = basename($bt[0]['file']);
			throw new Exception("Invalid parameter value passed", $bt['args'], __FUNCTION__);
		}
		return $html;
	}

	/**
	 *
	 * @param type $file_name
	 * @param type $data
	 * @param type $write_mode
	 * @return boolean
	 * @version 0.1.0
	 * @since 0.2.3
	 * @internal Development Status = Golden!
	 */
	function writeToFile( $file_name, $data, $write_mode=NULL ) {
		// Author: Dana Byrd
		// Version: 0.1.0.3
		// Note: "w+" truncates the file to zero, that is, deletes everything in the file before writing.
		// If the file doesn't exists, it create a new file.
		if(empty($write_mode)){
			$write_mode_arg = "w";
			// Alternate modes a+, w+
		} elseif($write_mode == "clear-then-write"){
			$write_mode_arg = "w+";
		} elseif($write_mode == "append-top" || $write_mode == "append"){
			$write_mode_arg = "a+";
		} else {
			return -1;
		}
		if(empty($file_name)) {
			return false;
		}
		// Verify that the folder exists
		$path    = pathinfo($file_name, PATHINFO_DIRNAME);
		if(! file_exists($path)) {
			//CreateFolder($path);
		}
		$fh = fopen($file_name, $write_mode_arg);
		if (!$fh) {
			echo __METHOD__ ."()<br />";
			//echo "<span style='color:red;font-weight:700;'>Error on file open </span>(<pre>$file_name</pre>)";
			echo "<br><br>$data<br><br>)";
			return -1;
		}
		$len_data = strlen($data);
		if($write_mode == "append-top"){
			rewind($fh);
		}
		fwrite($fh, $data, $len_data);
		fclose($fh);
		return true;
	}

	/**
	 * Create function to bypass the strict error that only vars should be passed by referen
	 * @param string $ref
	 * @return string $branchName
	 * @internal Development Status = Golden!
	 * @version 0.1.0
	 * @since 0.2.3
	 */
	public function getBranchFromRef($ref) {
		$refParts = explode('/',$ref);
		if (count($refParts) > 0) {
			$branchName = $refParts[ count($refParts) - 1];
		} else {
			$branchName = '';
		}
		return $branchName;
	}

	/**
	 *
	 * @throws Exception
	 * @internal Development Status = Golden!
	 * @version 0.1.0
	 * @since 0.2.3
	 */
	public function auth() {

		if (! isset($this->password))	{
			$this->fatalError('ERROR: (fatal) A password property is REQUIRED for all deployment objects');
		}

		if (empty($_REQUEST['p'])) {
			$this->fatalError('ERROR: (fatal) Deployment request is missing hook password');
		}

		if ($_REQUEST['p'] !== $this->password) {
			$this->fatalError('ERROR: Invalid hook password passed by the caller');
		}
	}

	/**
	 * Insure that all fatal errors are reported to the developer
	 * @param string $msg
	 * @internal Development Status = ready for testing (rft)
	 */
	public function fatalError($msg) {
		$this->logAppend($msg);
		$this->processStatusSummary();
		die();
	}

    /**
     * Added by dbyrd
     * See also: Semantic Versioning Specification (SemVer)
     *   http://semver.org/
     * Sets the semantic version number in three levels of verbosity.
     * Adds the version strings as a property of this class
     * Usage: Yii::app()->['version_info']
     */
    protected function getVersionNumber($site, $versionStrType='short')
    {
        if (! \file_exists($this->target_site_protected_folder)){
                return;
        }elseif (file_exists($this->target_site_protected_folder)){
                $target = $this->target_site_protected_folder;
        } else {
                $target = addSlash($site['web_root']) . addSlash($site['site_root']) . 'protected/main.php';
        }

        //$version_info = $this->configWeb['params']['version_info'];
        $deployedAppConfigArray = file_get_contents($target);
        if (!isset($deployedAppConfigArray['params']['version_info'])){
                // record error
        }
        $version_info = $deployedAppConfigArray['params']['version_info'];
        $version_string = "";
        //$version_suffix = "";
        $version_string = $version_info['major']
                . '.' . $version_info['minor']
                . '.' . $version_info['patch'];
        $version_suffix_short   = "";
        $version_suffix_long    = "";
        //$version_suffix_verbose = "";
        $testing_status = $version_info['user_testing_status'];
        switch ($testing_status) {
            case 'alpha':
                $version_suffix_short = $version_info['suffix_alpha'];
                $version_suffix_long  = $testing_status;
                break;
            case 'beta':
                $version_suffix_short = $version_info['suffix_beta'];
                $version_suffix_long  = $testing_status;
                break;
            case 'RC':
                $version_suffix_short = $version_info['suffix_release_candidate']
                    .$version_info['release_candidate_iteration'];
                $version_suffix_long  = $testing_status;
                break;
            case 'production':
                $version_suffix_short = '';
                $version_suffix_long  = '';
                break;
            default:
                throw new Exception("Invalid version information stored in the Environment array.");
        }

        $this->deployedAppVersionNumberShort   = $version_string . '' . $version_suffix_short;
        $this->deployedAppVersionNumberLong    = $version_string . ' ' . $version_suffix_long;
        $this->deployedAppVersionNumberVerbose = $version_string . ' ' . $version_suffix_long
                . ' - Dev Status: ' . $version_info['app_life_cycle_status'] . '';

        if ($versionStrType=='short'){
                $out = $this->deployedAppVersionNumberShort;
        } elseif ($versionStrType=='long'){
                $out = $this->deployedAppVersionNumberLong;
        } elseif ($versionStrType=='verbose'){
                $out = $this->deployedAppVersionNumberVerbose;
        } else {
                $out = $this->deployedAppVersionNumberVerbose;
        }
        return $out;
    }

}