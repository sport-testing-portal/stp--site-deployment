This project explores the use of a proprietary git hooks responder written in PHP and bash shell script in contrast to **compose deploy**.


In contrast is an exploration of using **composer deploy** as a methodology for deployment. 

The **proprietary deploy** version 

* requires SSH
* requires root access

## Features
* Can set set extremely precise permissions on all deployment folders since it runs as root on the target server where the source is being deployed 
* Uses hashdeep 256 **forensic two-way data compare** to verify that all files are in place as they should be and nothing else exists where it shouldn't be
* Enforces an extremely **'strict' deployment** locked down with the bare minimun Linux file and folder permissions to run the site effectively